﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace LambdaOperations
{
    public class Emloyee
    {
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public string Department { get; set; }

        public Emloyee(int id, string name, string department)
        {
            EmpID = id;
            EmpName = name;
            Department = department;
        }
        public override string ToString()
        {
            return EmpName + " " + EmpID + " " + Department;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Emloyee> employees = new List<Emloyee>();
            int itID = 0;
            employees.Add(new Emloyee(++itID, "Hany", "depA"));
            employees.Add(new Emloyee(++itID, "Hany", "depA"));
            employees.Add(new Emloyee(++itID, "Will", "IT"));
            employees.Add(new Emloyee(++itID, "Sam", "IT"));
            employees.Add(new Emloyee(++itID, "Hany", "depA"));
            employees.Add(new Emloyee(++itID, "Hany", "IT"));
            employees.Add(new Emloyee(++itID, "Hany", "HR"));


            foreach (var emp in employees)
            {
                Console.WriteLine(emp);
            }

            Action<Emloyee> print = e => Console.WriteLine(e.ToString());

            Console.WriteLine();
            employees.ForEach(print);

            // list of employees working in id department 
            Console.WriteLine("\nList of employees working in IT department");
            employees.FindAll(e => e.Department == "IT").ForEach(print);

            // sorted list employee
            Console.WriteLine("Sorted");
            employees.Sort((e1, e2) => e1.EmpName.CompareTo(e2.EmpName));
            employees.ForEach(print);


            // Last employee got hired in the hr department
            Console.WriteLine("Last employee hired in hr");
            Console.WriteLine(employees.FindLast(e => e.Department == "HR"));


            // Another example
            Action<string> greet = name =>
            {
                string greeting = $"Hello {name}";
                Console.WriteLine(greeting);
            };

            greet("Info-5101 class");

            Func<double, double, List<double>> rectangle = (length, width) =>
           {
               var area = length * width;
               var perimeter = (length + width) * 2;
               List<double> recdata = new List<double>();
               recdata.Add(area);
               recdata.Add(perimeter);

               return recdata;
           };

            rectangle(4, 5).ForEach(item => Console.WriteLine(item));
            // or even -- Thanks to hunter
            rectangle(4, 5).ForEach(Console.WriteLine);

            // or we could do this...
            foreach (var item in rectangle(4, 5))
            {
                Console.WriteLine(item);
            }

            // Expression tree
            Func<int, int, int> add = (a, b) => a + b;
            Expression <Func<int, int, int>> expression = (a, b) => a + b;

            Console.WriteLine($"Expression parameters count: {expression.Parameters.Count}" );
            Console.WriteLine($"Expression body:  {expression.Body}");

            var compiled = expression.Compile();
            Console.WriteLine(compiled(4, 5));


            // create binary tree
            var leftNode = Expression.Constant(3);
            var rightNode = Expression.Constant(5);

            var multiplyExpr = Expression.Multiply(leftNode, rightNode );
            Console.WriteLine($"Node {multiplyExpr.NodeType} of type {multiplyExpr.Type} and it holds {multiplyExpr.ToString()}");

            // first way to run or compile this tree
            Console.WriteLine(Expression.Lambda<Func<int>>(multiplyExpr).Compile()());

            // second way
            var exprCompiled = Expression.Lambda<Func<int>>(multiplyExpr).Compile();
            Console.WriteLine($"The result = {exprCompiled.Invoke()}");

        }
    }
}
